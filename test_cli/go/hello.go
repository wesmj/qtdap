package main

import "fmt"

func main() {
	f := "Hello World %v"
	var v int = 3
	fmt.Println(v)
	var i int = 101
	var j int = 2
	v = i + j
	fmt.Println("i", i)
	fmt.Println("j", j)
	fmt.Println("v", v)
	for idx := 1; idx <= 3; idx++ {
		fmt.Printf(f, v + idx)
		fmt.Println()
	}
}
