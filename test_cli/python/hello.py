#!/usr/bin/env python
import sys


class Prueba:

    def __init__(self):
        self.campo1 = 1
        self.campo2 = 2


def main():
    valor = 1
    otro = 2
    nulo = None
    lista = [1, 2, 3, 4]
    prueba = Prueba()
    mapa = {
        "cosa": 1,
        "adios": 2,
    }
    print("ARGV:", sys.argv)
#    raise Exception("prueba")
#    breakpoint()
    res = otro + valor
    print("valor", res)
    print("fin")


if __name__ == "__main__":
    main()
