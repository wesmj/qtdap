#!/usr/bin/env perl
#

use strict;

print STDOUT "ARGS:";
print STDOUT " [$_]" foreach @ARGV;
print STDOUT "\n";

sub myfunction {
	print("function");
}

my $color = 'red';
print "Your favorite color is "  . $color ."\n";

my $a = 10;
my $b = 20;
myfunction();
my $c = $a + $b;
print($c);

