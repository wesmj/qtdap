#!/bin/bash
BREAKPOINTS="breakpoints.json"
PROGRAM="python/hello.py"
ARGS="primer_arg segundo_arg"

../src/build/qt-dap $1 -b "$BREAKPOINTS" python launch +file "$PROGRAM" +args $ARGS
