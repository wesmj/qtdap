#!/bin/bash
BREAKPOINTS="breakpoints.json"
PROGRAM="perl/hello.pl"
LOCALLIB="$HOME/perl5/lib/perl5"
ARGS="primer_arg segundo_arg"

../src/build/qt-dap $1 -b "$BREAKPOINTS" perl launch_local +locallib "${LOCALLIB}" +file "$PROGRAM" +args "$ARGS"
