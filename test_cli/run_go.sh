#!/bin/bash
BREAKPOINTS="breakpoints.json"
PROGRAM="go/hello.go"

../src/build/qt-dap $1 -b "$BREAKPOINTS" go launch +file "$PROGRAM"
