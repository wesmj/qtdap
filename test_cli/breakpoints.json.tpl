{
	"breakpoints": {
		"${root}/python/hello.py": [
			{
				"line": 1
			},
			{
				"line": 9
			},
			{
				"line": 25
			},
			{
				"line": 2
			},
			{
				"line": 31
			}
		],
		"${root}/perl/hello.pl": [
			{
				"line": 1
			},
			{
				"line": 6
			},
			{
				"line": 7
			},
			{
				"line": 10
			}
		],
		"${root}/go/hello.go": [
			{
				"line": 3
			},
			{
				"line": 6
			},
			{
				"line": 2
			},
			{
				"line": 16,
				"condition": "idx == 2"
			}
		]
	},
	"function_breakpoints": {
	},
	"exception_breakpoints": {}
}
