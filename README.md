
Implementation of a client interface for Debugger Adapter Protocol ((DAP)[https://microsoft.github.io/debug-adapter-protocol]), spec 2021, based on Qt.

A demo command line client is provided.

The library can interact with a DAP server in 3 ways:

- Managed subprocess, communicating by standard IO channels.
- Managed subprocess, communicating by TCP socket.
- Unamanaged remote server, communicating by TCP socket.

## Supported/Unsupported features

### Launching and terminating

* [X] Attach
* [X] Launch
* [X] Configuration done
* [X] Disconnect
* [X] Exited event
* [X] Terminate
* [ ] Terminate threads
* [ ] Restart
* [X] Process event
* [X] Stopped event

### Inspection

* [ ] Breakpoints locations
* [ ] Completions
* [ ] Disassemble
* [X] Evaluate
* [X] Modules
* [X] Stack trace
* [X] Scopes
* [X] Threads
* [X] Thread event
* [X] Variables
* [ ] Exception info
* [ ] Set variable
* [ ] Set expression
* [X] Output event

### Memory

* [ ] Read memory
* [ ] Write memory
* [ ] Invalidated event
* [ ] Memory event

### Movement

* [X] Goto
* [X] Next
* [X] Step in
* [X] Step out
* [X] Continue
* [X] Continued event
* [ ] Pause
* [ ] Step back
* [X] Stopped event
* [ ] Reverse continue
* [ ] Restart frame

### Progress

* [ ] Progress end event
* [ ] Progress start event
* [ ] Progress update event

### Source

* [X] Source
* [ ] Loaded sources

### Breakpoints

* [X] Breakpoints
* [X] Breakpoint event
* [X] Conditional breakpoints
* [X] Hit conditional breakpoints
* [ ] Data breakpoints
* [ ] Exception breakpoints
* [ ] Instruction breakpoints

## Server settings templates

Each server requires two sections defined:

- run: server invocation and supported protocol features.
- configurations: a map with different ways to debug a program using that server

### Run sections

Options:

`command`: `[string]`
:   Server process invocation.

`environment: {string: string}`
:   Environment variables for the process invocation. If not defined, use system defined variables.

`host: string`
:   Server host. If not defined, "127.0.0.1"

`port: int`
:   Server port. If `port == 0`, a random port will be used.

`redirectStderr: bool`
:   Redirect standard error output to output events with category==console. By default, `false`.

`redirectStdout: bool`
:   Redirect standard output to output events with category==stdout. By default, `false`.

`supportsSourceRequest: bool`
:   Set to `true` if the server supports the DAP source request.


If `command` and a socket variable (`host`, `port`) are defined together: run process and listen to socket.


### Configurations

In each configuration section, global commands can be overriden.

Required variable:

`request`: `{string: any}`
:   Arguments required by the server implementation of `launch` or `attach` requests.

`request.command`: `launch|attach`

Options:

`commandArgs`: `[string]`
:   Append this array to the `command` variable.


### Placeholders

A generalised configuration can be created by using variable placeholders:

`${variable}`
:   Replace with the string value of a defined variable called `variable`.

`${variable|base}`
:   Replace with the string value of a defined variable called `variable`, 
    interpret it as a filename and remove the extension. 
    
`${variable|dir}`
:   Replace with the string value of a defined variable called `variable`,
    interpret it as a filename and remove the file part.
    
If the placeholder template is the only content in the string, it can be used
to cast to other variable types:

`${variable|int}`
:   Convert `variable` to integer.

`${variable|bool}`
:   Convert `variable` to boolean.

`${variable|list}`
:   Convert `variable` to string list


There are two special variables:

`${#run.port}`
:   Use the value of `port` variable defined in the run/configuration section.

`${#run.host}`
:   Use the value of `host` variable defined in the run/configuration section.


### Example

```
{
    "servers": {
        "go": {
            "run": {
                "command": ["dlv", "dap", "--listen", "127.0.0.1:${#run.port}"],
                "port": 0,
                "redirectStderr": true,
                "redirectStdout": true,
                "supportsSourceRequest": false
            },
            "configurations": {
                "launch": {
                    "request": {
                        "command": "launch",
                        "mode": "debug",
                        "program": "${file}",
                        "args": "${args|list}",
                        "stopOnEntry": true
                    }
                }
            }
        }
    }
}
```

## Command line client

```
Usage: qt-dap [options] server configuration [arguments...]

Options:
  -h, --help                       Displays this help.
  --breakpoints, -b <breakpoints>  breakpoints file
  --debug, -d                      debug

Arguments:
  server                           server definition
  configuration                    server's specific configuration
  arguments                        configuration variables

```

Variables required by the configuration template are provided using the following syntax:

```
+var1 value_var1 +var2 value_var2_1 value_var2_2
```

Example:

```
qt-dap -b breakpoints.json python launch +file hello.py +args firstvalue secondvalue
```

### Breakpoints file

```
{
    "breakpoints": {
        "path": [{"line": 1}, {"line": 3}, {"line": 5, "condition": "counter == 4"}]
    }
}
```
