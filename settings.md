
# run

process:
`command: [string]`
`environment: {string: string}`

socket:
`host: string`
`port: int`

If both `command` and a socket variable (`host`, `port`) are defined: run process and listen to socket.

If `port == 0`, use a random high port.

# waitForInitializationEvent

# configurations

`command`
`commandArgs`
`environment`
`host`
`port`
`request`: request

`request.command` == launch|attach

# placeholders

`${variable}`: string
`${variable|base}`: no extension
`${variable|dir}`: directory

`${variable|int}`: convert to integer
`${variable|bool}`: convert to bool
`${variable|list}`: convert to string list

# references

`${#run.port}`
`${#run.host}`


TODO: placeholders
