/*
    SPDX-FileCopyrightText: 2022 Héctor Mesa Jiménez <wmj.py@gmx.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/
#ifndef CLI_CLI_H
#define CLI_CLI_H

#include <utility>
#include <memory>
#include <QObject>
#include <QList>
#include <QMultiHash>
#include <QCommandLineParser>
#include "dap/bus.h"
#include "dap/processbus.h"
#include "dap/client.h"

class QCommandLineParser;

class CLI: public QObject
{
    Q_OBJECT
public:

    enum State {
        Running,
        Stopped,
        Terminated
    };

    static void setParser(QCommandLineParser &parser);
    explicit CLI(QObject *parent = nullptr);
    bool loadFromArguments(const QCommandLineParser& parser);
    void run();
    void onRunning();
signals:
    void finished();
private:
    void setClient(const QJsonObject& conf);
    void onFinished();
    void onInitialized();
    void onThreads(const QList<dap::Thread> &threads);
    void onModules(const dap::ModulesInfo &modules);
    void onStopped(const dap::StoppedEvent &info);
    void onStackTrace(const int threadId, const dap::StackTraceInfo &info);
    void onScopes(const int frameId, const QList<dap::Scope> &scopes);
    void onVariables(const int variablesReference, const QList<dap::Variable> &variables);
    void onSourceContent(const QString& path, const int reference, const dap::SourceContent& content);
    void onSourceBreakpoints(const QString& path, const int reference, const std::optional<QList<dap::Breakpoint>>& breakpoints);
    void onTerminated();
    void onExpressionEvaluated(const QString& expression, const std::optional<dap::EvaluateInfo>& info);
    void onGotoTargets(const dap::Source& source, const int line, const QList<dap::GotoTarget>& targets);

    void resetState();
    void waitForUser();
    void addRequest();
    void removeRequest();
    bool processUserCommand(const QString& line);

    bool cmdHelp();
    bool cmdQuit();
    bool cmdVariables();
    bool cmdModules();
    bool cmdStackTrace();
    bool cmdThreads();
    bool cmdScopes();
    bool cmdUpStack();
    bool cmdDownStack();
    bool cmdNext(const QString& command);
    bool cmdStepIn(const QString& command);
    bool cmdStepOut(const QString& command);
    bool cmdContinue(const QString& command);
    bool cmdTerminate();
    bool cmdList(const QString& command);
    bool cmdBreakpointsList();
    bool cmdBreakpointOff(const QString& command);
    bool cmdBreakpointOn(const QString& command);
    bool cmdWatch(const QString& command);
    bool cmdWatchList();
    bool cmdGoto(const QString& command);

    bool removeBreakpoint(const int id);
    void moveFrame();
    void getSource();
    void printSourceContext(int line);

    dap::settings::ClientSettings m_settings;
    std::unique_ptr<dap::Bus> m_bus;
    std::unique_ptr<dap::Client> m_client;

    int m_requests;
    State m_state;
    std::optional<int> m_currentThread;
    std::optional<int> m_currentFrame;
    int m_contextWidth;

    QHash<int, dap::Thread> m_threads;
    QList<dap::Module> m_modules;
    QList<dap::StackFrame> m_frames;
    QList<dap::Scope> m_scopes;
    QHash<int, QList<dap::Variable>> m_variables;
    QString m_sourceId;
    QStringList m_sourceContent;
    QHash<QString, dap::Source> m_sources;
    QMap<QString, QList<std::optional<dap::Breakpoint>>> m_breakpoints;
    QMap<QString, QList<dap::SourceBreakpoint>> m_wantedBreakpoints;
    QHash<QString, std::optional<dap::EvaluateInfo>> m_watch;
};

#endif // CLI_CLI_H
