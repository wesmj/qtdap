/*
    SPDX-FileCopyrightText: 2022 Héctor Mesa Jiménez <wmj.py@gmx.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "cli.h"

#include <utility>
#include <QTextStream>
#include <QJsonDocument>
#include <QDebug>
#include <QStringList>
#include <QFile>
#include <QJsonArray>
#include <QMetaEnum>
#include <QTimer>
#include <QFileInfo>
#include <QDir>
#include <QSettings>
#include <QRegularExpression>
#include "dap/bus_selector.h"
#include "dap/settings.h"
#include "json_placeholders.h"

#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
#define ENDL endl
#else
#define ENDL Qt::endl
#endif

constexpr int CONTEXT = 6;

static QTextStream sout(stdout);
static QTextStream serr(stderr);
static QTextStream sin(stdin);


QTextStream& ts(const dap::Output::Category cat){
    switch(cat){
    case dap::Output::Category::Stderr:
        return serr;
    case dap::Output::Category::Stdout:
        return sout;
    default:
        serr << "(" << QMetaEnum::fromType<dap::Output::Category>().valueToKey(int(cat)) << ") ";
        return serr;
    }
}

QTextStream& l_info(){
    serr << "[DAP] << ";
    return serr;
}

QTextStream& l_event(){
    serr << "[DAP] EVT ";
    return serr;
}

QTextStream& l_out(const QString& prefix){
    sout << "[" << prefix << "] ";
    return sout;
}

bool closeVariable(const QString& variable, QStringList& parts, QHash<QString, QJsonValue> &varMap){
    if(variable.isEmpty() && !parts.isEmpty()){
        qCritical() << "Arguments not associated to variable: " << parts;
        return false;
    }
    if(!variable.isEmpty()){
        if(varMap.contains(variable)){
            qCritical() << "Multiple definition of variable: " << variable;
            return false;
        }
        varMap.insert(variable, QJsonArray::fromStringList(parts));
        parts.clear();
    }
    return true;
}

bool parseProgramVariables(
    const QStringList &arguments,
    QHash<QString, QJsonValue> &varMap,
    const int skip
){
    QStringList parts;
    QString currentVariable;

    int n_item = 0;
    for(const auto& arg: arguments){
        ++n_item;
        if(n_item <= skip)
            continue;
        if(arg.startsWith(QChar('+'))){
            // close previous variable
            if(!closeVariable(currentVariable, parts, varMap))
                return false;
            // add to new variable
            currentVariable = arg.mid(1);
        }else{
            // add part
            parts << arg;
        }
    }

    return closeVariable(currentVariable, parts, varMap);
}

void CLI::setParser(QCommandLineParser &parser)
{
    parser.addHelpOption();
    parser.addOption({
        {"breakpoints", "b"}, QStringLiteral("breakpoints file"), QStringLiteral("breakpoints")
    });
    parser.addOption({{"debug", "d"}, "debug"});
    parser.addPositionalArgument(QStringLiteral("server"), QStringLiteral("server definition"));
    parser.addPositionalArgument(QStringLiteral("configuration"), QStringLiteral("server's specific configuration"));
    parser.addPositionalArgument(QStringLiteral("arguments"), QStringLiteral("configuration variables"), QStringLiteral("[arguments...]"));
}

CLI::CLI(QObject *parent):
    QObject(parent),
    m_state(State::Terminated),
    m_currentThread(std::nullopt),
    m_currentFrame(std::nullopt),
    m_contextWidth(CONTEXT)
{
    resetState();
}

std::optional<QJsonDocument> loadJSON(const QString& path){
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)){
        qCritical() << "file error: " << file.errorString();
        return std::nullopt;
    }
    QJsonParseError error;
    const auto json = QJsonDocument::fromJson(file.readAll(), &error);
    file.close();
    if(error.error != QJsonParseError::NoError){
        qCritical() << "JSON parse error: " << error.errorString();
        return std::nullopt;
    }
    return json;
}

std::optional<QJsonObject> loadServers(){
    const auto json = loadJSON(":/servers.json");
    if(!json)
        return std::nullopt;

    return json->object()[QStringLiteral("servers")].toObject();
}

static const QString F_BREAKPOINTS = QStringLiteral("breakpoints");


QString absolutePath(const QString& path){
    const QFileInfo fi(path);
    if(fi.isRelative()){
        const auto& cwd = QDir(QDir::currentPath());
        return cwd.absoluteFilePath(path);
    }
    return path;
}

std::optional<QMap<QString, QList<dap::SourceBreakpoint>>> loadBreakpoints(const QString& path){
    const auto json = loadJSON(path);
    if(!json)
        return std::nullopt;

    const auto map = json->object()[F_BREAKPOINTS].toObject();
    if(map.isEmpty())
        return std::nullopt;

    QMap<QString, QList<dap::SourceBreakpoint>> out;

    for(auto it=map.begin();it!=map.end();++it){
        QList<dap::SourceBreakpoint> points;
        for(const auto &bp: it.value().toArray()){
            points << dap::SourceBreakpoint(bp.toObject());
        }
        out[absolutePath(it.key())] = std::move(points);
    }
    return out;
}

QMap<QString, QList<dap::SourceBreakpoint>> loadBreakpointsFromSettings(){

    QMap<QString, QList<dap::SourceBreakpoint>> out;

    QSettings settings;

    qDebug() << "SETTINGS " << settings.fileName();

    const int size = settings.beginReadArray("breakpoints");
    for(int idx = 0; idx < size; ++idx){
        settings.setArrayIndex(idx);
        dap::SourceBreakpoint bp;
        if(!settings.contains("path") || !settings.contains("line")){
            continue;
        }
        const auto path = absolutePath(settings.value("path").toString());
        if(!out.contains(path)){
            out[path] = QList<dap::SourceBreakpoint>();
        }
        bool ok = false;
        bp.line = settings.value("line").toInt(&ok);
        if(!ok) continue;

        // condition
        if(settings.contains("condition")){
            bp.condition = settings.value("condition").toString();
        }

        out[path] << std::move(bp);
    }
    settings.endArray();

    return out;
}

void insertBreakpoint(QSettings& settings, const QString& path, const dap::SourceBreakpoint& bp){
    settings.setValue("path", path);
    settings.setValue("line", bp.line);
    if(bp.condition){
        settings.setValue("condition", bp.condition.value());
    }
}

void insertBreakpointIntoSettings(const QString& path, const dap::SourceBreakpoint& bp){
    QSettings settings;

    const int size = settings.beginReadArray("breakpoints");
    settings.endArray();
    settings.beginWriteArray("breakpoints");
    settings.setArrayIndex(size);
    insertBreakpoint(settings, path, bp);
    settings.endArray();
}

void updateSettingsBreakpoints(const QMap<QString, QList<dap::SourceBreakpoint>>& tables){
    QSettings settings;

    settings.remove("breakpoints");
    settings.beginWriteArray("breakpoints");
    int idx = 0;
    for(auto it=tables.begin(); it != tables.end(); ++it){
        for(const auto& bp: it.value()){
            settings.setArrayIndex(idx);
            insertBreakpoint(settings, it.key(), bp);
            ++idx;
        }
    }
    settings.endArray();
}

void showAvailableServers(const QJsonObject &servers){
    qInfo();
    qInfo() << "Available servers:";
    qInfo();
    for(const auto& server: servers.keys()){
        qInfo() << "* " << server;
    }
    qInfo();
}

void showAvailableConfs(const QJsonObject &server){
    qInfo();
    qInfo() << "Available configurations:";
    qInfo();
    for(const auto& conf: server[QStringLiteral("configurations")].toObject().keys()){
        qInfo() << "* " << conf;
    }
    qInfo();
}

bool CLI::loadFromArguments(const QCommandLineParser &parser)
{
    const auto& positionals = parser.positionalArguments();

    const auto servers = loadServers();
    if(!servers)
        return false;

    // no server
    if(positionals.isEmpty()){
        qCritical() << "No server specified";
        showAvailableServers(*servers);
        return false;
    }

    const auto& serverName = positionals.at(0);

    // find server
    const auto server = servers->value(serverName).toObject();
    if(server.isEmpty()){
        qCritical() << "Server " << serverName << " not found";
        showAvailableServers(*servers);
        return false;
    }

    qInfo() << "SERVER: " << serverName;

    // find configuration
    if(positionals.size() < 2){
        qCritical() << "No configuration specified for server " << serverName;
        showAvailableConfs(server);
        return false;
    }

    const auto &confName = positionals.at(1);
    auto conf = dap::settings::findConfiguration(server, confName, true);
    if(!conf){
        qCritical() << "Configuration " << confName << " not found in server " << serverName;
        showAvailableConfs(server);
        return false;
    }

    qInfo() << "CONFIGURATION: " << confName;

    QSet<QString> requiredVariables;
    json::findVariables(*conf, requiredVariables);

    // find variables
    auto variableMap = dap::settings::findReferences(*conf);
    if(!parseProgramVariables(positionals, variableMap, 2)){
        return false;
    }
    bool incomplete = false;
    if(!variableMap.isEmpty()){
        qInfo() << ENDL;
        qInfo() << "Required variables:";
        for(const auto& var: requiredVariables){
            if(variableMap.contains(var)){
                qInfo() << var << "=" << variableMap[var];
            }else{
                qInfo() << var << " NOT FOUND";
                incomplete = true;
            }
        }
        qInfo() << ENDL;
    }

    if(incomplete){
        qCritical() << "Configuration template could not be resolved.";
        return false;
    }

    *conf = json::resolve(*conf, variableMap);

    qDebug() << *conf;

    // initial breakpoints
    if(parser.isSet(F_BREAKPOINTS)){
        auto breakpoints = loadBreakpoints(parser.value(F_BREAKPOINTS));
        if(breakpoints){
            m_wantedBreakpoints.swap(*breakpoints);
        }else{
            qCritical() << "Breakpoints definition file not loaded";
            return false;
        }
    }else{
        auto breakpoints = loadBreakpointsFromSettings();
        if(!breakpoints.isEmpty()){
            m_wantedBreakpoints.swap(breakpoints);
        }
    }

    // load configuration
    setClient(*conf);

    return true;
}

void printOutput(const dap::Output& output){
    ts(output.category) << output.output;
    if(output.isSpecialOutput() || (output.output.indexOf('\n') > 0)){
        ts(output.category).flush();
    }
}

void printErrorResponse(const QString &summary, const std::optional<dap::Message> &message)
{
    serr << ENDL;
    auto &line = l_info();
    line << "error on response: " << summary;
    if(message){
        line << " {code " << message->id << ": " << message->format << "}";
    }
    line << ENDL << ENDL;
    line.flush();
}

void printProcess(const dap::ProcessInfo &info){
    serr << ENDL;
    auto &line = l_event();
    line << "debugging process ";
    if(info.systemProcessId){
        line << "[" << info.systemProcessId.value() << "]";
    }
    line << info.name;
    if(info.startMethod){
        line << " (start method: " << info.startMethod.value() << ")";
    }
    line << ENDL << ENDL;
    line.flush();
}

void printExited(int code){
    serr << ENDL;
    auto &line = l_event();
    line << "debuggee exited with code " << code << ENDL << ENDL;
    line.flush();
}

void printThreadEvent(const dap::ThreadEvent &info){
    serr << ENDL;
    auto &line = l_event();
    line << "thread " << info.threadId << " " << info.reason << ENDL << ENDL;
    line.flush();
}

void printCapabilities(const dap::Capabilities &cap){
    serr << ENDL;
    l_info() << "server capabilities" << ENDL;
    l_info() << "\t* supports configuration done request = " << cap.supportsConfigurationDoneRequest << ENDL;
    l_info() << "\t* supports conditional breakpoints = " << cap.supportsConditionalBreakpoints << ENDL;
    l_info() << "\t* supports function breakpoints = " << cap.supportsFunctionBreakpoints << ENDL;
    l_info() << "\t* supports hit conditional breakpoints = " << cap.supportsHitConditionalBreakpoints << ENDL;
    l_info() << "\t* supports log points = " << cap.supportsLogPoints << ENDL;
    l_info() << "\t* supports module request = " << cap.supportsModulesRequest << ENDL;
    l_info() << "\t* supports terminate request = " << cap.supportsTerminateRequest << ENDL;
    l_info() << "\t* supports terminate debuggee = " << cap.supportTerminateDebuggee << ENDL;
    l_info() << "\t* supports goto targets request = " << cap.supportsGotoTargetsRequest << ENDL;
    serr << ENDL;
    serr.flush();
}

void printThread(QTextStream &ts, const dap::Thread &thread){
    ts << thread.id << ": " << thread.name << ENDL;
}

void printModule(QTextStream &ts, const dap::Module &module){
    ts << "module ";
    if(module.id_int){
        ts << module.id_int.value();
    }else if(module.id_str){
        ts << module.id_str.value();
    }
    ts << ": " << module.name;
    if(module.isOptimized and module.isOptimized.value()){
        ts << " [optimized]";
    }
    if(module.path){
        ts << ": " << module.path.value() << ENDL;
    }
}

void printBreakpoint(
    QTextStream &ts,
    const QString& sourceId,
    const dap::SourceBreakpoint& def,
    const std::optional<dap::Breakpoint>& bp,
    const int bId
){
    ts << "[" << bId << ".";
    if(!bp){
        ts << " ";
    }else{
        if(bp->verified){
            if(bp->id){
                ts << bp->id.value();
            }else{
                ts << "?";
            }
        }else{
            ts << "!";
        }
    }
    ts << "] ";
    ts << sourceId << ":" << def.line;
    if(def.column){
        ts << "," << def.column.value();
    }
    if(bp){
        if(bp->line){
            ts << "->" << bp->line.value();
            if(bp->endLine){
                ts << "-" << bp->endLine.value();
            }
            if(bp->column){
                ts << "," << bp->column.value();
                if(bp->endColumn){
                    ts << "-" << bp->endColumn.value();
                }
            }
        }
    }
    if(def.condition){
        ts << " when {" << def.condition.value() << "}";
    }
    if(bp && bp->message){
        ts << " (" << bp->message.value() << ")";
    }
    ts << ENDL;
}

void printModuleEvent(const dap::ModuleEvent &info){
    serr << ENDL;
    auto line = std::ref(l_event());
    line.get() << "module " << info.reason << ENDL;
    line = std::ref(l_event());
    line.get() << "\t* ";
    printModule(line.get(), info.module);
    serr << ENDL;
    serr.flush();
}

void printFrame(QTextStream &ts, const dap::StackFrame &frame){
    ts <<  frame.id << ": " << frame.name;
    if(frame.source){
        if(!frame.source->origin.isEmpty()){
            ts << " (" << frame.source->origin << ")";
        }
        ts << " at";
        if(!frame.source->name.isEmpty()){
            ts << " {" << frame.source->name << "}";
        }
        ts << " " << frame.source->path;
    }
    ts << ":" << frame.line << "," << frame.column;
    if(frame.moduleId_int || frame.moduleId_str){
        ts << " module: ";
        if(frame.moduleId_int){
            ts << frame.moduleId_int.value();
        }else{
            ts << frame.moduleId_str.value();
        }
    }
    ts << ENDL;
}

void printScope(QTextStream &ts, const dap::Scope &scope){
    ts << "scope " << scope.name << " (ref. " << scope.variablesReference << ")";
    if(scope.namedVariables){
        ts << scope.namedVariables.value() << " named vars";
    }
    if(scope.indexedVariables){
        ts << scope.indexedVariables.value() << "indexed vars";
    }
    ts << ENDL;
}

void printVariable(QTextStream &ts, const dap::Variable &variable){
    ts << "variable '" << variable.name << "'";
    if(variable.namedVariables){
        ts << "{" << variable.namedVariables.value() << "}";
    }
    if(variable.indexedVariables){
        ts << "[" << variable.indexedVariables.value() << "]";
    }
    if(variable.type && !variable.type->isEmpty()){
        ts << ": " << variable.type.value();
    }
    ts << " = " << variable.value;
    if(variable.variablesReference > 0){
        ts << " (ref. " << variable.variablesReference << ")";
    }
    if(variable.evaluateName){
        ts << "; eval name '" << variable.evaluateName.value() << "'";
    }
    ts << ENDL;
}

void printWatch(QTextStream& ts, const QString& expression, std::optional<dap::EvaluateInfo>& info){
    ts << expression << " = ";
    if(!info){
        ts << "???";
    }else{
        if(info->type){
            ts << "[" << info->type.value() << "] ";
        }
        ts << info->result;
    }
    ts << ENDL;
}

void printStackTrace(const int threadId, const dap::StackTraceInfo &info)
{
    int total = info.totalFrames.value_or(0);
    if(total == 0){
        total = info.stackFrames.size();
    }
    serr << ENDL;
    auto line = std::ref(l_info());
    line.get() << "stacktrace for thread " << threadId;
    line.get() << " (" << info.stackFrames.size() << "/" << total << " frame(s))";
    line.get() << ENDL;
    for(const auto& item: info.stackFrames){
        line = std::ref(l_info());
        line.get() << "\t* ";
        printFrame(line.get(), item);

    }
    serr << ENDL;
    serr.flush();
}

void printScopes(const int frameId, const QList<dap::Scope> &scopes)
{
    serr << ENDL;
    auto line = std::ref(l_info());
    line.get() << " scope(s) for frame " << frameId << ENDL;
    for(const auto& scope: scopes){
        line = std::ref(l_info());
        line.get() << "\t* ";
        printScope(line.get(), scope);
    }
    serr << ENDL;
    serr.flush();
}

void printVariables(const int variablesReference, const QList<dap::Variable> &variables)
{
    serr << ENDL;
    auto line = std::ref(l_info());
    line.get() << variables.size() << " variable(s) for reference " << variablesReference << ENDL;
    for(const auto& variable: variables){
        line = std::ref(l_info());
        line.get() << "\t* ";
        printVariable(line.get(), variable);
    }
    serr << ENDL;
    serr.flush();
}

void printModules(const dap::ModulesInfo &modules)
{
    int total = modules.totalModules.value_or(0);
    if(total == 0){
        total = modules.modules.size();
    }
    serr << ENDL;
    auto line = std::ref(l_info());
    line.get() << modules.modules.size() << "/" << total << " module(s)" << ENDL;
    for(const auto& module: modules.modules){
        line = std::ref(l_info());
        line.get() << "\t* ";
        printModule(line.get(), module);
    }
    serr << ENDL;
    serr.flush();
}

void printThreads(const QList<dap::Thread> &threads)
{
    serr << ENDL;
    auto &line = l_info();
    line << "total " << threads.size() << " thread(s)" << ENDL;
    for(const auto& item: threads){
        auto &line = l_info();
        line << "\t* ";
        printThread(line, item);
    }
    serr << ENDL;
    serr.flush();
}

void printContinuedEvent(const dap::ContinuedEvent& info){
    serr << ENDL;
    auto& line = l_info();
    line << "thread " << info.threadId << " continued";
    if(info.allThreadsContinued){
        line << " (all threads continued)";
    }
    line << ENDL;
    line.flush();
};

int findEndLine(const QString& text, int start){
    const int pos = text.indexOf(QStringLiteral("\n"), start);
    if(pos < 0)
        return text.size();
    return pos;
}

void CLI::printSourceContext(int line){
    if(m_sourceContent.isEmpty())
        return;

    int context = m_contextWidth;

    if(context < 1){
        context = m_sourceContent.size();
    }
    const int start = std::max(line - context, 1);
    const int end = std::min(line + context, m_sourceContent.size() - 1);

    // breakpoints
    const auto& bpoints = m_breakpoints[m_sourceId];
    QSet<int> btable;
    for(const auto& bp: bpoints){
        if(!bp) continue;
        if(!bp->line) continue;
        btable.insert(bp->line.value());
    }

    for(int current = start; current <= end; ++current){
        auto& ts = l_out(QStringLiteral("source"));
        if(line == current){
            ts << "-> ";
        }else if(btable.contains(current)){
            ts << " * ";
        }else{
            ts << "   ";
        }
        ts << QString::number(current).rightJustified(3);
        ts << ":";
        ts << m_sourceContent.at(current - 1);
        ts << ENDL;
    }

    sout.flush();
}

void printBreakpointEvent(const dap::BreakpointEvent& info){
    serr << ENDL;
    auto& line = l_info();
    line << "breakpoint " << info.reason << " | ";
    if(info.breakpoint.source){
        line << info.breakpoint.source->unifiedId();
    }
    if(info.breakpoint.line){
        line << ":" << info.breakpoint.line.value();
    }
    line << ENDL;
    line.flush();
}

void CLI::setClient(const QJsonObject &conf){
    m_settings = dap::settings::ClientSettings(conf);

    // create bus
    m_bus.reset(dap::createBus(m_settings.busSettings));
    // create client
    m_client.reset(new dap::Client(m_settings.protocolSettings, m_bus.get()));

    connect(m_client.get(), &dap::Client::debuggeeRunning, this, &CLI::onRunning);
    connect(m_client.get(), &dap::Client::finished, this, &CLI::onFinished);
    connect(m_client.get(), &dap::Client::failed, this, &CLI::cmdQuit);
    connect(m_client.get(), &dap::Client::initialized, this, &CLI::onInitialized);
    connect(m_client.get(), &dap::Client::errorResponse, &printErrorResponse);
    connect(m_client.get(), &dap::Client::outputProduced, &printOutput);
    connect(m_client.get(), &dap::Client::debuggingProcess, &printProcess);
    connect(m_client.get(), &dap::Client::debuggeeTerminated, this, &CLI::onTerminated);
    connect(m_client.get(), &dap::Client::debuggeeExited, &printExited);
    connect(m_client.get(), &dap::Client::capabilitiesReceived, &printCapabilities);
    connect(m_client.get(), &dap::Client::threadChanged, &printThreadEvent);
    connect(m_client.get(), &dap::Client::moduleChanged, &printModuleEvent);
    connect(m_client.get(), &dap::Client::debuggeeStopped, this, &CLI::onStopped);
    connect(m_client.get(), &dap::Client::debuggeeRunning, []{
        serr << ENDL;
        auto& line = l_event();
        line << "running" << ENDL << ENDL;
        serr.flush();
    });
    connect(m_client.get(), &dap::Client::debuggeeDisconnected, m_bus.get(), &dap::Bus::close);
    connect(m_client.get(), &dap::Client::debuggeeContinued, &printContinuedEvent);
    connect(m_client.get(), &dap::Client::threads, this, &CLI::onThreads);
    connect(m_client.get(), &dap::Client::stackTrace, this, &CLI::onStackTrace);
    connect(m_client.get(), &dap::Client::scopes, this, &CLI::onScopes);
    connect(m_client.get(), &dap::Client::variables, this, &CLI::onVariables);
    connect(m_client.get(), &dap::Client::modules, this, &CLI::onModules);
    connect(m_client.get(), &dap::Client::sourceContent, this, &CLI::onSourceContent);
    connect(m_client.get(), &dap::Client::sourceBreakpoints, this, &CLI::onSourceBreakpoints);
    connect(m_client.get(), &dap::Client::breakpointChanged, &printBreakpointEvent);
    connect(m_client.get(), &dap::Client::expressionEvaluated, this, &CLI::onExpressionEvaluated);
    connect(m_client.get(), &dap::Client::gotoTargets, this, &CLI::onGotoTargets);
}

void CLI::onFinished()
{
    serr << ENDL;
    serr.flush();
    sout << ENDL;
    sout.flush();
    emit finished();
}

void CLI::onInitialized()
{
    serr << ENDL;
    l_event() << "initialized... configure now" << ENDL << ENDL;
    for(auto it=m_wantedBreakpoints.begin(); it!=m_wantedBreakpoints.end(); ++it){
        addRequest();
        m_client->requestSetBreakpoints(it.key(), it.value());
    }
    m_client->requestConfigurationDone();
    serr << ENDL;
    l_event() << "configuration done." << ENDL << ENDL;
    serr.flush();
}

void CLI::onThreads(const QList<dap::Thread> &threads)
{
    removeRequest();

//    printThreads(threads);
    for(const auto& thread: threads){
        m_threads[thread.id] = thread;
    }

    waitForUser();
}

void CLI::onModules(const dap::ModulesInfo &modules)
{
    removeRequest();

//    printModules(modules);
    m_modules = modules.modules;

    waitForUser();
}

void CLI::onTerminated(){
    m_state = State::Terminated;

    serr << ENDL;
    auto &line = l_event();
    line << "debuggee terminated" << ENDL << ENDL;
    line.flush();

    cmdQuit();
}

void CLI::onExpressionEvaluated(const QString &expression, const std::optional<dap::EvaluateInfo> &info)
{
    removeRequest();

    m_watch[expression] = std::move(info);

    waitForUser();
}

void CLI::onGotoTargets(const dap::Source &, const int, const QList<dap::GotoTarget> &targets)
{
    removeRequest();

    if(!targets.isEmpty() && m_currentThread){
        m_client->requestGoto(*m_currentThread, targets[0].id);
        return;
    }

    waitForUser();
}

void CLI::onStopped(const dap::StoppedEvent &info)
{
    m_state = State::Stopped;
    m_currentThread = info.threadId;

    serr << ENDL;
    auto& line = l_event();
    line << "stopped";
    if(info.threadId){
        line << " thread " << *m_currentThread;
    }
    if(info.allThreadsStopped && info.allThreadsStopped.value()){
        line << " (all threads stopped)";
    }
    line << " [" << info.reason << "]";
    if(info.description){
        line << " " << info.description.value();
    }
    if(info.hitBreakpointsIds){
        line << " at breakpoint(s) ";
        for(const int b: info.hitBreakpointsIds.value()){
            line << "[" << b << "] ";
        }
    }
    line << ENDL << ENDL;
    line.flush();

    // threads
    if(m_currentThread){
        addRequest();
        m_client->requestStackTrace(*m_currentThread);
    }

    addRequest();
    m_client->requestThreads();
    // modules
    if(m_client->adapterCapabilities().supportsModulesRequest){
        addRequest();
        m_client->requestModules();
    }

    waitForUser();
}

void CLI::onStackTrace(const int /*threadId*/, const dap::StackTraceInfo &info)
{
    removeRequest();

//    printStackTrace(threadId, info);

    m_frames = std::move(info.stackFrames);

    if(!m_frames.isEmpty()){
        m_currentFrame = 0;
        addRequest();
        const int frameId = m_frames[*m_currentFrame].id;
        m_client->requestScopes(frameId);

        // watched expressions
        if(!m_watch.isEmpty()){
            for(const auto& expression: m_watch.keys()){
                addRequest();
                m_client->requestWatch(expression, frameId);
            }
        }
    }
    waitForUser();
}

void CLI::onScopes(const int /*frameId*/, const QList<dap::Scope> &scopes)
{
    removeRequest();

//    printScopes(frameId, scopes);

    m_scopes = scopes;

    for(const auto& scope: scopes){
        addRequest();
        m_client->requestVariables(scope.variablesReference);
    }

    waitForUser();
}

void CLI::onVariables(const int variablesReference, const QList<dap::Variable> &variables)
{
    removeRequest();

//    printVariables(variablesReference, variables);
    m_variables[variablesReference] = variables;

    waitForUser();
}

void CLI::onSourceContent(const QString &path, const int reference, const dap::SourceContent &content)
{
    removeRequest();

    const auto id = dap::Source::getUnifiedId(path, reference);
    if(!id.isEmpty()){
        m_sourceId = id;
        if(!content.content.isEmpty()){
            m_sourceContent = content.content.split(QStringLiteral("\n"));
        }else if(!path.isEmpty()){
            m_sourceContent = dap::SourceContent(path).content.split(QStringLiteral("\n"));
        }

        if(m_currentFrame && !m_sourceContent.isEmpty()){
            const auto& frame = m_frames[*m_currentFrame];
            printSourceContext(frame.line);
        }
    }

    waitForUser();
}

void CLI::onSourceBreakpoints(const QString& path, int reference, const std::optional<QList<dap::Breakpoint>> &breakpoints)
{
    removeRequest();
    if(breakpoints){
        const auto id = dap::Source::getUnifiedId(path, reference);
        if(!id.isEmpty()){
            if(!m_breakpoints.contains(id)){
                m_breakpoints[id] = QList<std::optional<dap::Breakpoint>>();
                m_breakpoints[id].reserve(breakpoints.value().size());
            }
            auto& table = m_breakpoints[id];
            int pointIdx = 0;
            const int last = table.size();
            for(const auto& point: *breakpoints){
                if(pointIdx >= last){
                    table << point;
                }else if(!table[pointIdx]){
                    table[pointIdx] = point;
                }
                ++pointIdx;
            }
        }
    }
    waitForUser();
}

void CLI::resetState()
{
    m_state = State::Running;
    m_requests = 0;

    m_currentThread = std::nullopt;
    m_currentFrame = std::nullopt;

    m_threads.clear();
    m_modules.clear();
    m_frames.clear();
    m_scopes.clear();
    m_variables.clear();
}

void CLI::waitForUser()
{
    if((m_state == State::Running) || (m_requests > 0))
        return;

    sout << ENDL;
    if(m_currentThread && m_threads.contains(*m_currentThread)){
        auto& line = l_info();
        line << "current thread: ";
        printThread(line, m_threads[*m_currentThread]);
    }
    if(m_currentFrame){
        auto& line = l_info();
        line << "current frame: ";
        printFrame(line, m_frames[*m_currentFrame]);
    }
    l_info() << "(type h for help)" << ENDL;
    sout << ENDL;

    QString line;
    do{
        if(sin.atEnd()){
            cmdQuit();
            return;
        }
        sout << "[DAP] >> ";
        sout.flush();
        sin.readLineInto(&line);
    }while(!processUserCommand(line));

    sout.flush();
}

void CLI::addRequest()
{
    ++m_requests;
}

void CLI::removeRequest()
{
    if(m_requests > 0){
        --m_requests;
    }
}

bool CLI::processUserCommand(const QString &line)
{
    const auto command = line.trimmed();
    if(command.isEmpty()){
        return false;
    }else if(command.startsWith(QChar('h'))){
        return cmdHelp();
    }else if(command.startsWith(QChar('q'))){
        return cmdQuit();
    }else if(command.startsWith(QChar('v'))){
        return cmdVariables();
    }else if(command.startsWith(QChar('m'))){
        return cmdModules();
    }else if(command.startsWith(QStringLiteral("f"))){
        return cmdStackTrace();
    }else if(command.startsWith(QStringLiteral("th"))){
        return cmdThreads();
    }else if(command.startsWith(QStringLiteral("sc"))){
        return cmdScopes();
    }else if(command.startsWith(QChar('u'))){
        return cmdUpStack();
    }else if(command.startsWith(QChar('d'))){
        return cmdDownStack();
    }else if(command.startsWith(QChar('n'))){
        return cmdNext(command);
    }else if(command.startsWith(QChar('i'))){
        return cmdStepIn(command);
    }else if(command.startsWith(QChar('o'))){
        return cmdStepOut(command);
    }else if(command.startsWith(QChar('c'))){
        return cmdContinue(command);
    }else if(command.startsWith(QChar('l'))){
        return cmdList(command);
    }else if(command.startsWith(QStringLiteral("bl"))){
        return cmdBreakpointsList();
    }else if(command.startsWith(QStringLiteral("boff"))){
        return cmdBreakpointOff(command);
    }else if(command.startsWith(QChar('b'))){
        return cmdBreakpointOn(command);
    }else if(command.startsWith(QStringLiteral("wl"))){
        return cmdWatchList();
    }else if(command.startsWith(QChar('w'))){
        return cmdWatch(command);
    }else if(command.startsWith(QChar('g'))){
        return cmdGoto(command);
    }else{
        sout << "Unknown command: " << command << ENDL;
        sout.flush();
    }
    return false;
}

bool CLI::cmdQuit()
{
    if((m_state != State::Terminated) && m_client->adapterCapabilities().supportsTerminateRequest){
        qDebug() << "TERMINATING";
        return cmdTerminate();
    }
    m_client->requestDisconnect();
    return true;
}

bool CLI::cmdHelp()
{
    sout << "Available commands:" << ENDL;
    sout << "\th[elp]";
    sout << "\tq[uit]";
    sout << ENDL << ENDL;
    sout << "\tc[ontinue]" << ENDL;
    sout << "\tn[ext] [threadId] (next/step)" << ENDL;
    sout << "\ti[n] [threadId] (step in)" << ENDL;
    sout << "\to[out] [threadId] (step out)" << ENDL;
    if(m_client->adapterCapabilities().supportsGotoTargetsRequest){
        sout << "\tg[oto] <line> [file]";
    }
    sout << ENDL << ENDL;
    sout << "\tth[reads]";
    if(m_client->adapterCapabilities().supportsModulesRequest){
        sout << "\tm[odules]";
    }
    sout << "\tf[rames]";
    sout << "\tsc[opes]";
    sout << "\tv[ariables]";
    sout << ENDL;
    sout << "\tu[p stack]";
    sout << "\td[own stack]";
    sout << ENDL;
    sout << ENDL;
    sout << "\tll (long list)" << ENDL;
    sout << "\tl[ist] <context>" << ENDL;
    sout << ENDL;
    sout << "\tbl[ist] (breakpoints)" << ENDL;
    sout << "\tb <line> [when {condition}] [on <source>]" << ENDL;
    sout << "\tboff <id>" << ENDL;
    sout << ENDL;
    sout << "\twl[ist] (expressions watched)" << ENDL;
    sout << "\tw[atch] <expression>" << ENDL;
    sout.flush();

    return false;
}

bool CLI::cmdVariables(){
    for(const auto& scope: m_scopes){
        printScope(l_out(QStringLiteral("scope")), scope);
        if(!m_variables.contains(scope.variablesReference))
            continue;
        for(const auto& variable: m_variables[scope.variablesReference]){
            auto& line = l_out(QStringLiteral("variable"));
            line << "\t* ";
            printVariable(line, variable);
        }
    }
    sout << ENDL;
    sout.flush();

    return false;
}

bool CLI::cmdBreakpointsList(){
    int bId = 0;
    for(auto it=m_breakpoints.begin(); it!=m_breakpoints.end(); ++it){
        const auto& sourceId = it.key();
        const auto& defs = m_wantedBreakpoints[sourceId];
        int pointIdx = 0;
        for(const auto& b: it.value()){
            const auto& def = defs[pointIdx];
            printBreakpoint(l_out(QStringLiteral("breakpoint")), sourceId, def, b, bId);
            ++pointIdx;
            ++bId;
        }
    }
    sout << ENDL;
    sout.flush();

    return false;
}

bool CLI::cmdModules(){
    for(const auto& module: m_modules){
        printModule(l_out(QStringLiteral("module")), module);
    }
    sout << ENDL;
    sout.flush();

    return false;
}

bool CLI::cmdThreads(){
    for(const auto& thread: m_threads){
        auto& line = l_out(QStringLiteral("thread"));
        if(m_currentThread && (*m_currentThread == thread.id)){
            line << "-> ";
        }else{
            line << "   ";
        }
        printThread(line, thread);
    }
    sout << ENDL;
    sout.flush();

    return false;
}

bool CLI::cmdStackTrace(){
    int idx = 0;
    for(const auto& frame: m_frames){
        auto& line = l_out(QStringLiteral("frame"));
        if(m_currentFrame && (*m_currentFrame == idx)){
            line << "-> ";
        }else{
            line << "   ";
        }
        printFrame(line, frame);

        ++idx;
    }
    sout << ENDL;
    sout.flush();

    return false;
}

bool CLI::cmdScopes(){
    for(const auto& scope: m_scopes){
        printScope(l_out(QStringLiteral("scope")), scope);
    }
    sout << ENDL;
    sout.flush();

    return false;
}

void CLI::moveFrame(){
    if(!m_currentFrame)
        return;
    m_scopes.clear();
    m_variables.clear();

    addRequest();
    m_client->requestScopes(m_frames[*m_currentFrame].id);

    waitForUser();
}

bool CLI::cmdUpStack(){
    if(!m_currentFrame)
        return false;
    if(m_currentFrame == (m_frames.size() - 1)){
        auto &line = l_info();
        line << "frame pointer already on the top" << ENDL;
        line.flush();
        return false;
    }

    ++(*m_currentFrame);
    moveFrame();

    return true;
}

bool CLI::cmdDownStack(){
    if(!m_currentFrame)
        return false;
    if(m_currentFrame == 0){
        auto &line = l_info();
        line << "frame pointer already on the bottom" << ENDL;
        line.flush();
        return false;
    }

    --(*m_currentFrame);
    moveFrame();

    return true;
}

QRegularExpression rx_next(R"--(^\s*[a-z]+(?:\s+(\d+))?\s*$)--");

bool CLI::cmdNext(const QString& command){
    const auto match = rx_next.match(command);
    if(!match.hasMatch()){
        l_info() << "next command has invalid format" << ENDL;
        serr.flush();
        return false;
    }
    int threadId;
    if(match.lastCapturedIndex() == 1){
        threadId = match.captured(1).toInt();
        if(!m_threads.contains(threadId)){
            l_info() << "next: thread " << threadId << " does not exist" << ENDL;
            serr.flush();
            return false;
        }
    }else if(m_currentThread){
        threadId = *m_currentThread;
    }else{
        l_info() << "next: threadId not specified" << ENDL;
        serr.flush();
        return false;
    }

    resetState();
    m_client->requestNext(threadId);

    return true;
}

bool CLI::cmdStepIn(const QString &command)
{
    const auto match = rx_next.match(command);
    if(!match.hasMatch()){
        l_info() << "in command has invalid format" << ENDL;
        serr.flush();
        return false;
    }
    int threadId;
    if(match.lastCapturedIndex() == 1){
        threadId = match.captured(1).toInt();
        if(!m_threads.contains(threadId)){
            l_info() << "in: thread " << threadId << " does not exist" << ENDL;
            serr.flush();
            return false;
        }
    }else if(m_currentThread){
        threadId = *m_currentThread;
    }else{
        l_info() << "in: threadId not specified" << ENDL;
        serr.flush();
        return false;
    }

    resetState();
    m_client->requestStepIn(threadId);

    return true;
}


bool CLI::cmdStepOut(const QString &command)
{
    const auto match = rx_next.match(command);
    if(!match.hasMatch()){
        l_info() << "out command has invalid format" << ENDL;
        serr.flush();
        return false;
    }
    int threadId;
    if(match.lastCapturedIndex() == 1){
        threadId = match.captured(1).toInt();
        if(!m_threads.contains(threadId)){
            l_info() << "out: thread " << threadId << " does not exist" << ENDL;
            serr.flush();
            return false;
        }
    }else if(m_currentThread){
        threadId = *m_currentThread;
    }else{
        l_info() << "out: threadId not specified" << ENDL;
        serr.flush();
        return false;
    }

    resetState();
    m_client->requestStepOut(threadId);

    return true;
}

bool CLI::cmdContinue(const QString &command){
    const auto match = rx_next.match(command);
    if(!match.hasMatch()){
        l_info() << "continue command has invalid format" << ENDL;
        serr.flush();
        return false;
    }
    int threadId;
    if(match.lastCapturedIndex() == 1){
        threadId = match.captured(1).toInt();
        if(!m_threads.contains(threadId)){
            l_info() << "continue: thread " << threadId << " does not exist" << ENDL;
            serr.flush();
            return false;
        }
    }else if(m_currentThread){
        threadId = *m_currentThread;
    }else{
        l_info() << "continue: threadId not specified" << ENDL;
        serr.flush();
        return false;
    }

    resetState();
    m_client->requestContinue(threadId);

    return true;
}

bool CLI::cmdTerminate(){
    if(!m_client->adapterCapabilities().supportsTerminateRequest){
        l_info() << "terminate request is not supported by the server" << ENDL;
        return false;
    }

    if(m_state == State::Terminated)
        return false;

    m_client->requestTerminate();

    return true;
}

bool CLI::cmdList(const QString& command){
    if(!m_currentFrame)
        return false;

    const auto& frame = m_frames[*m_currentFrame];
    if(!frame.source){
        l_info() << "no source available" << ENDL;
        return false;
    }

    const auto id = frame.source->unifiedId();
    if(id.isEmpty()){
        l_info() << "no source available" << ENDL;
        return false;
    }

    if(command.startsWith("ll")){
        m_contextWidth = 0;
    }else{
        const auto match = rx_next.match(command);
        if(!match.hasMatch()){
            l_info() << "list command has invalid format" << ENDL;
            serr.flush();
            return false;
        }

        if(!match.captured(1).isNull()){
            bool ok = false;
            int intValue = match.captured(1).toInt(&ok);
            if(ok){
                m_contextWidth = intValue;
            }
        }else{
            m_contextWidth = CONTEXT;
        }
    }

    if((m_sourceId == id) && m_sourceContent.isEmpty()){
        printSourceContext(frame.line);
        return false;
    }else if(!m_client->protocol().supportsSourceRequest){
        QTimer::singleShot(0,[this, frame]{
            onSourceContent(frame.source->path, frame.source->sourceReference.value_or(0), dap::SourceContent());
        });
        return true;
    }

    addRequest();
    m_client->requestSource(frame.source.value());

    return true;
}

bool CLI::removeBreakpoint(const int id){
    int bId = 0;
    for(auto source=m_breakpoints.begin(); source!=m_breakpoints.end(); ++source){
        int idx = 0;
        for(const auto& bp: source.value()){
            if(bId == id){
                auto& table = m_wantedBreakpoints[source.key()];
                const bool isEnabled = bp.has_value();
                table.removeAt(idx);
                source.value().removeAt(idx);
                updateSettingsBreakpoints(m_wantedBreakpoints);
                if(isEnabled){
                    addRequest();
                    m_client->requestSetBreakpoints(source.key(), table);

                    return true;
                }else{
                    return false;
                }
            }
            ++idx;
            ++bId;
        }
    }
    return false;
}

QRegularExpression rx_boff(R"--(^boff\s+(\d+)\s*$)--");

bool CLI::cmdBreakpointOff(const QString &command){
    const auto match = rx_boff.match(command);
    if(!match.hasMatch()){
        l_info() << "boff command has invalid format" << ENDL;
        serr.flush();
        return false;
    }
    const auto& txtId = match.captured(1);
    bool ok = false;
    const int id = txtId.toInt(&ok);
    if(!ok){
        l_info() << "invalid breakpoint id " << txtId << ENDL;
        serr.flush();
        return false;
    }

    // remove
    return removeBreakpoint(id);
}

QRegularExpression rx_bon(R"--(^b\s+(\d+)(?:\s+when\s+\{(?P<COND>.+)\})?(?:\s+on\s+(?P<SOURCE>.+))?\s*$)--");

bool CLI::cmdBreakpointOn(const QString &command){
    const auto match = rx_bon.match(command);
    if(!match.hasMatch()){
        l_info() << "b command has invalid format" << ENDL;
        serr.flush();
        return false;
    }
    const auto& txtLine = match.captured(1);
    bool ok = false;
    const int line = txtLine.toInt(&ok);
    if(!ok){
        l_info() << "invalid line " << txtLine << ENDL;
        serr.flush();
        return false;
    }

    dap::SourceBreakpoint bp(line);

    bp.condition = match.captured(QStringLiteral("COND"));
    if(bp.condition->isNull()){
        bp.condition = std::nullopt;
    }
    QString path = match.captured(QStringLiteral("SOURCE"));
    if(path.isNull()){
        if(!m_currentFrame){
            l_info() << "required source" << ENDL;
            serr.flush();
            return false;
        }
        const auto& frame = this->m_frames[*m_currentFrame];
        if(!frame.source){
            l_info() << "required source" << ENDL;
            serr.flush();
            return false;
        }
        path = absolutePath(frame.source->unifiedId());
    }else{
        path = absolutePath(path);
    }

    insertBreakpointIntoSettings(path, bp);

    m_wantedBreakpoints[path] << std::move(bp);
    m_breakpoints[path] << std::nullopt;

    addRequest();
    m_client->requestSetBreakpoints(path, m_wantedBreakpoints[path]);
    return true;
}


QRegularExpression rx_goto(R"--(^g[a-z]*\s+(\d+)(?:\s+(\S+))?$)--");

bool CLI::cmdGoto(const QString& command)
{
    const auto match = rx_goto.match(command);
    if(!match.hasMatch()){
        l_info() << "goto command has invalid format" << ENDL;
        serr.flush();
        return false;
    }

    const auto& txtLine = match.captured(1);
    bool ok = false;
    const int line = txtLine.toInt(&ok);
    if(!ok){
        l_info() << "invalid line " << txtLine << ENDL;
        serr.flush();
        return false;
    }

    QString path = match.captured(2);
    if(path.isNull()){
        if(!m_currentFrame){
            l_info() << "required source" << ENDL;
            serr.flush();
            return false;
        }
        const auto& frame = this->m_frames[*m_currentFrame];
        if(!frame.source){
            l_info() << "required source" << ENDL;
            serr.flush();
            return false;
        }
        path = frame.source->unifiedId();
    }

    addRequest();
    m_client->requestGotoTargets(path, line);

    return true;
}

bool CLI::cmdWatch(const QString &command)
{
    const int start = command.indexOf(" ");
    if(start < 0){
        l_info() << "watch: expression not found" << ENDL;
        serr.flush();
        return false;
    }
    const auto expression = command.mid(start).trimmed();
    if(expression.isEmpty()){
        l_info() << "watch: expression not found" << ENDL;
        serr.flush();
        return false;
    }

    m_watch[expression] = std::nullopt;

    addRequest();
    std::optional<int> frameId = std::nullopt;
    if(m_currentFrame)
        frameId = m_frames[*m_currentFrame].id;
    m_client->requestWatch(expression, frameId);
    return true;
}

bool CLI::cmdWatchList()
{
    for(auto it=m_watch.begin(); it!=m_watch.end(); ++it){
        auto& line = l_out("watch");
        printWatch(line, it.key(), it.value());
    }
    sout << ENDL;
    sout.flush();
    return false;
}

void CLI::onRunning()
{}

void CLI::run() {
    m_bus->start(m_settings.busSettings);
}
