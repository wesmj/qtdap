/*
    SPDX-FileCopyrightText: 2022 Héctor Mesa Jiménez <wmj.py@gmx.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/
#ifndef DAP_LOGGING_H
#define DAP_LOGGING_H

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(DAPCLIENT)

#endif // DAP_LOGGING_H
