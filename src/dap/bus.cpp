/*
    SPDX-FileCopyrightText: 2022 Héctor Mesa Jiménez <wmj.py@gmx.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/
#include "bus.h"

#include "logging.h"
//Q_LOGGING_CATEGORY(DAPCLIENT, "dapclient")

namespace dap {

Bus::Bus(QObject *parent) :
    QObject(parent),
    m_state(State::None)
{
}

Bus::State Bus::state() const { return m_state; }

void Bus::setState(State state) {
    if(state == m_state)
        return;
    m_state = state;

    emit stateChanged(state);

    switch(state){
    case State::Running:
        emit running();
        break;
    case State::Closed:
        emit closed();
        break;
    default: ;
    }
}

}



