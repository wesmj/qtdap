#include <QCoreApplication>
#include <QObject>
#include <QTimer>
#include <QLoggingCategory>
#include <QCommandLineParser>

#include "cli/cli.h"

Q_LOGGING_CATEGORY(DAPCLIENT, "dapclient");

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    app.setApplicationName("qtdap");
    app.setOrganizationName("wmj");
//    Q_INIT_RESOURCE(resources);

    QCommandLineParser parser;
    CLI::setParser(parser);    
    parser.process(app);

    if(!parser.isSet(QStringLiteral("debug"))){
        QLoggingCategory::setFilterRules("dapclient.debug=false");
    }

    CLI cli;

    bool ok = cli.loadFromArguments(parser);
    if(!ok)
        return 1;

    app.connect(&cli, &CLI::finished, &app, &QCoreApplication::quit);

    QTimer::singleShot(0, &cli, &CLI::run);
    return app.exec();
}
